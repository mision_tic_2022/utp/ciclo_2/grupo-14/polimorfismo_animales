package com.polimorfismo_animales;

public class App 
{
    public static void main( String[] args )
    {
        Animal[] animales = new Animal[3];

        animales[0] = new Rana(20, 1, false, "Verde", 800, false);
        animales[1] = new Pez(15, 1, false, "Gris", 600);
        animales[2] = new Perro(80, 2, true, "Cafe", 20000000, "Criollo");

        /**
         * For mejorado ó 
         * foreach
         */
        for (Animal animal : animales) {
            animal.desplazar();
        }

    }
}
