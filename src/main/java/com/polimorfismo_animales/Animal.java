package com.polimorfismo_animales;

public class Animal {
    //ATRIBUTOS
    private double tamanio;
    private double edad;
    private boolean mamifero;
    private String color;
    private double peso;

    //CONSTRUCTOR
    public Animal(double tamanio, double edad, boolean mamifero, String color, double peso) {
        this.tamanio = tamanio;
        this.edad = edad;
        this.mamifero = mamifero;
        this.color = color;
        this.peso = peso;
    }

    //CONSULTORES

    public double getTamanio() {
        return tamanio;
    }

    public double getEdad() {
        return edad;
    }

    public boolean isMamifero() {
        return mamifero;
    }

    public String getColor() {
        return color;
    }

    public double getPeso() {
        return peso;
    }

    //MODIFICADORES

    public void setTamanio(double tamanio) {
        this.tamanio = tamanio;
    }

    public void setEdad(double edad) {
        this.edad = edad;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    //ACCIONES
    public void comer(){
        System.out.println("Comiendo...");
    }

    public void dormir(){
        System.out.println("Durmiendo...zzz");
    }

    public void desplazar(){
        System.out.println("Desplazandose...");
    }

}
