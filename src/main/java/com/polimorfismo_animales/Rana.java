package com.polimorfismo_animales;

public class Rana extends Animal {
    //ATRIBUTOS
    private boolean venenosa;

    //CONSTRUCTOR
    public Rana(double tamanio, double edad, boolean mamifero, String color, double peso, boolean venenosa){
        super(tamanio, edad, mamifero, color, peso);
        this.venenosa = venenosa;
    }

    //CONSULTOR
    public boolean isVenenosa(){
        return venenosa;
    }

    //MODIFICADOR
    public void setVenenosa(boolean venenosa){
        this.venenosa = venenosa;
    }

    //POLIMORFISMO
    @Override
    public void desplazar() {
        System.out.println("Saltar, saltar, saltar, saltar");
    }
}
