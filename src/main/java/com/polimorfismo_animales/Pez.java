package com.polimorfismo_animales;

public class Pez extends Animal {

    //CONSTRUCTOR
    public Pez(double tamanio, double edad, boolean mamifero, String color, double peso) {
        super(tamanio, edad, mamifero, color, peso);
    }

    //POLIMORFISMO
    @Override
    public void desplazar() {
        System.out.println("Nadar...");
    }

}
