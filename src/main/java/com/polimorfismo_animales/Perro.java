package com.polimorfismo_animales;

public class Perro extends Animal {
    //ATRIBUTOS
    private String raza;

    //CONSTRUCTOR
    public Perro(double tamanio, double edad, boolean mamifero, String color, double peso, String raza) {
        super(tamanio, edad, mamifero, color, peso);
        this.raza = raza;
    }

    //CONSULTOR
    public String getRaza(){
        return raza;
    }

    //MODIFICADOR
    public void setRaza(String raza){
        this.raza = raza;
    }

    //ACCIONES
    public void ladrar(){
        System.out.println("Güau, Güau, Güau, Güau, Güau");
    }

    //POLIMORFISMO
    @Override
    public void desplazar() {
        System.out.println("Caminar...");
    }

}
